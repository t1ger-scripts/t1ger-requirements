# T1GER Requirements

My versions of requirements/scripts/dependencies used throughout my t1ger-scripts collection.

All credits go to original author/developer of each script.
I simply just added them into one repository to make it easier for customers to get the dependencies/requirements. 
In some of the scripts I have also made few changes/edits. 